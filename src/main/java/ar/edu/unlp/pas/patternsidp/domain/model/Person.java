package ar.edu.unlp.pas.patternsidp.domain.model;

import ar.edu.unlp.pas.patternsidp.domain.model.validators.ValidateBirth;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Person {

  public Person(
      final String name,
      final String lastName,
      final LocalDate dateOfBirth,
      final String email,
      final Address invoiceAddress,
      final List<Address> shippingAddress,
      final String password,
      final List<Role> roles
  ) {
    this.name = name;
    this.lastName = lastName;
    this.dateOfBirth = dateOfBirth;
    this.email = email;
    this.invoiceAddress = invoiceAddress;
    this.shippingAddress = shippingAddress;
    this.password = password;
    this.roles = roles;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @NotBlank(message = "Name is mandatory")
  private String name;

  @JsonProperty("last_name")
  @NotBlank(message = "Last Name is mandatory")
  private String lastName;

  @JsonProperty("password")
  private String password;

  @JsonProperty("date_of_birth")
  @ValidateBirth(min = 18, max=125, message="Must be between 18 and 125 years old")
  private LocalDate dateOfBirth;

  @Column(unique=true)
  @NotBlank(message = "Email is mandatory")
  @Email(message = "Email should be valid")
  private String email;

  @JsonProperty("invoice_address")
  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "invoice_address_id", referencedColumnName = "id")
  @NotNull(message = "Invoice Address is mandatory")
  private Address invoiceAddress;

  @JsonProperty("shipping_address")
  @OneToMany(cascade = CascadeType.ALL)
  @NotNull(message = "Shipping Address is mandatory")
  @Size(min=1, message = "Should be at least one shipping address")
  private List<Address> shippingAddress;

  @ManyToMany(cascade=CascadeType.ALL,fetch= FetchType.EAGER)
  @JoinTable(name="USER_ROLES",
      joinColumns = {@JoinColumn (name="USER_ID", referencedColumnName="id")},
      inverseJoinColumns = {@JoinColumn(name="ROLE_ID", referencedColumnName="id")}
  )
  private Collection<Role> roles = new ArrayList<>();
}

