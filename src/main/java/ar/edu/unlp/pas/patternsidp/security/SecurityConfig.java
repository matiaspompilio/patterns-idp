package ar.edu.unlp.pas.patternsidp.security;

import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

import ar.edu.unlp.pas.patternsidp.domain.service.AuthService;
import ar.edu.unlp.pas.patternsidp.filter.CustomAuthenticationFilter;
import ar.edu.unlp.pas.patternsidp.filter.CustomAuthorizationFilter;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

@EnableWebSecurity
@Component
@Configuration
@AllArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  @Autowired
  private final AuthService userDetailService;
  @Autowired
  private final BCryptPasswordEncoder bCryptPasswordEncoder;

  @Override
  protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailService).passwordEncoder(bCryptPasswordEncoder);
  }

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    CustomAuthenticationFilter customAuthenticationFilter = new CustomAuthenticationFilter(authenticationManagerBean());
    customAuthenticationFilter.setFilterProcessesUrl("/api/v1/login");
    // h2 database authentication
    http.csrf().disable();
    http.headers().frameOptions().disable();
    http.sessionManagement().sessionCreationPolicy(STATELESS);
    http.authorizeRequests().antMatchers("POST", "/api/v1/login/**", "/api/v1/token/refresh/**", "/v3/api-docs/**",
      "/swagger-ui/**", "/swagger-ui.html").permitAll();
    http.authorizeRequests().antMatchers(GET, "/api/v1/person/**").hasAnyAuthority(Roles.ROLE_USER, Roles.ROLE_ADMIN, Roles.ROLE_DELIVERY);
    http.authorizeRequests().antMatchers(GET, "/api/v1/auth/**").hasAnyAuthority(Roles.ROLE_USER, Roles.ROLE_ADMIN, Roles.ROLE_DELIVERY);
    http.authorizeRequests().antMatchers(POST, "/api/v1/person/**").hasAnyAuthority(Roles.ROLE_ADMIN);
    http.authorizeRequests().antMatchers(PUT, "/api/v1/person/**").hasAnyAuthority(Roles.ROLE_ADMIN);
    http.authorizeRequests().antMatchers(DELETE, "/api/v1/person/**").hasAnyAuthority(Roles.ROLE_ADMIN);
    http.authorizeRequests().anyRequest().authenticated();
    http.addFilter(customAuthenticationFilter);
    http.addFilterBefore(new CustomAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
  }

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }
}
