## patterns-idp

# Como ejecutar la aplicación con Docker
1. Asegurarse de tener Docker instalado en el sistema.

2. Abrir una terminal y navegar hasta la carpeta raíz del proyecto que contiene el Dockerfile.

3. Una vez confirmado que el Dockerfile está presente, puedes construir la imagen de Docker usando el siguiente comando:

   ```
   docker build -t nombre_imagen .
   ```

   Se puede reemplazar `nombre_imagen` con el nombre que desees darle a la imagen de Docker. El punto (.) al final del comando indica que el contexto de compilación es el directorio actual.

4. Después de que se complete la construcción de la imagen, para ejecutar un contenedor basado en esa imagen se utiliza el siguiente comando:

   ```
   docker run -p 8080:8080 nombre_imagen
   ```

   Esto mapea el puerto 8080 del contenedor al puerto 8080 de tu máquina host.


# Swagger
1. Una vez la aplicación esté en ejecución, deberías poder acceder a la aplicación Swagger desde el navegador web usando la siguiente URL: http://localhost:8080/swagger-ui/index.html

   Si todo va bien, deberías ver la interfaz de usuario de Swagger y podrás interactuar con la API de tu aplicación.

Importante: Asegurarse de que el puerto de la url de la aplicación es el correcto en donde se está corriendo.
