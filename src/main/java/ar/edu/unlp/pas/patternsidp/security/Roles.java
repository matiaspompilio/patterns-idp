package ar.edu.unlp.pas.patternsidp.security;

public interface Roles {
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_USER = "ROLE_USER";
    public static final String ROLE_DELIVERY = "ROLE_DELIVERY";

}
