package ar.edu.unlp.pas.patternsidp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ResourceAlreadyExist extends RuntimeException {
  public ResourceAlreadyExist(final String resourceName, final String fieldName, final Object fieldValue) {
    super(String.format("Object type %s finding by %s using value: '%s' already exist",
        resourceName, fieldName, fieldValue));
  }
}
