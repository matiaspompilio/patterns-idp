package ar.edu.unlp.pas.patternsidp.domain.service;

import ar.edu.unlp.pas.patternsidp.domain.model.Person;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class AuthService implements UserDetailsService {
  @Autowired
  private final PersonService personService;
  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    Person person = personService.getByEmail(email);
    final List<SimpleGrantedAuthority>
        authorities = person.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
    return new User(person.getEmail(), person.getPassword(), authorities);
  }
}

