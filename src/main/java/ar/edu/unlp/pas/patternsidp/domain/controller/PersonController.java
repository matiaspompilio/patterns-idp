package ar.edu.unlp.pas.patternsidp.domain.controller;


import ar.edu.unlp.pas.patternsidp.domain.dto.PersonDTO;
import ar.edu.unlp.pas.patternsidp.domain.model.Person;
import ar.edu.unlp.pas.patternsidp.domain.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/person")
public class PersonController {

    private final PersonService personService;

    @GetMapping("/me")
    public ResponseEntity<PersonDTO> getByToken() {
        return ResponseEntity.ok(personService.getMe());
    }


    @GetMapping
    public ResponseEntity<List<PersonDTO>> getAll() {
        return ResponseEntity.ok(
            personService.getAll()
        );
    }

    @GetMapping("{personId}")
    public ResponseEntity<PersonDTO> getById(@PathVariable Long personId) {
        return ResponseEntity.ok(personService.getById(personId));
    }

    @PostMapping
    public ResponseEntity<PersonDTO> registerNewPerson(@RequestBody final Person person) {
        return ResponseEntity.ok(personService.registerNew(person));
    }

    @DeleteMapping(path = "{personId}")
    public void deletePerson(@PathVariable("personId") Long personId) {
        personService.delete(personId);
    }

    @PutMapping
    public ResponseEntity<PersonDTO> replacePerson(@RequestBody final Person person) {
        return ResponseEntity.ok(personService.replacePerson(person));
    }

}
