package ar.edu.unlp.pas.patternsidp.domain.dto.mapper;

import ar.edu.unlp.pas.patternsidp.domain.dto.AddressDTO;
import ar.edu.unlp.pas.patternsidp.domain.dto.PersonDTO;
import ar.edu.unlp.pas.patternsidp.domain.model.Address;
import ar.edu.unlp.pas.patternsidp.domain.model.Person;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PersonMapper {

  PersonMapper INSTANCE = Mappers.getMapper(PersonMapper.class);

  @Mapping(target = "fullName", expression = "java(person.getName() + ' ' + person.getLastName())")
  @Mapping(target = "age", source = "dateOfBirth", qualifiedByName = "dateOfBirthToAge")
  @Mapping(target = "invoiceAddress", source = "invoiceAddress", qualifiedByName = "invoiceAddress")
  @Mapping(target = "shippingAddress", source = "shippingAddress", qualifiedByName = "shippingAddress")
  PersonDTO personToPersonDto(Person person);

  @Named("dateOfBirthToAge")
  static Integer dateOfBirthToAge(final LocalDate dateOfBirth) {
    return Period.between(dateOfBirth, LocalDate.now()).getYears();
  }

  @Named("invoiceAddress")
  static AddressDTO address(Address address) {
    return AddressMapper.INSTANCE.addressToAddressDTO(address);
  }

  @Named("shippingAddress")
  static List<AddressDTO> shippingAddress(List<Address> addresses) {
    return addresses.stream().map(PersonMapper::address).collect(Collectors.toList());
  }
}

