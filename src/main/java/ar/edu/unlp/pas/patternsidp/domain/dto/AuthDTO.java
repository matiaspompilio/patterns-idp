package ar.edu.unlp.pas.patternsidp.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AuthDTO {
    String requestUri;
}
