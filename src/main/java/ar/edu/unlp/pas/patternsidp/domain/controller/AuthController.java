package ar.edu.unlp.pas.patternsidp.domain.controller;


import ar.edu.unlp.pas.patternsidp.domain.dto.AuthDTO;
import ar.edu.unlp.pas.patternsidp.security.EndpointsConfig;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/auth")
public class AuthController {

    @PostMapping("/authorize")
    public ResponseEntity<Boolean> authorize(@RequestBody AuthDTO body) {
        List<String> rolesForEndpoint = EndpointsConfig.getRolesForEndpoint(body.getRequestUri());

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) authentication.getAuthorities();

        Boolean authorized = authorities.stream().anyMatch(element -> rolesForEndpoint.contains(element.toString()));;
        return ResponseEntity.ok(authorized);
    }

}

