package ar.edu.unlp.pas.patternsidp.domain.dto.mapper;

import ar.edu.unlp.pas.patternsidp.domain.dto.AddressDTO;
import ar.edu.unlp.pas.patternsidp.domain.model.Address;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface AddressMapper {
    AddressMapper INSTANCE = Mappers.getMapper(AddressMapper.class);

    AddressDTO addressToAddressDTO(Address address);
}
