package ar.edu.unlp.pas.patternsidp.security;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class EndpointsConfig {
    public static Map<String, List<String>> getConfig() {

        Map<String, List<String>> config = new HashMap<>();

        config.put("/api/v1/publication/*", List.of(Roles.ROLE_DELIVERY, Roles.ROLE_USER, Roles.ROLE_ADMIN));
        config.put("/api/v1/delivery/*", List.of(Roles.ROLE_DELIVERY, Roles.ROLE_ADMIN, Roles.ROLE_USER));
        config.put("/api/v1/carts/*", List.of(Roles.ROLE_USER, Roles.ROLE_ADMIN));

        return config;
    }

    public static List<String> getRolesForEndpoint(String uri) {
        for (Map.Entry<String, List<String>> entry : EndpointsConfig.getConfig().entrySet()) {
            if (uri.contains(entry.getKey().split("/\\*")[0])) {
                return entry.getValue();
            }
        }
        return new ArrayList<>();
    }
}
