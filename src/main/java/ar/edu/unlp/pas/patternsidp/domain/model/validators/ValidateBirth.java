package ar.edu.unlp.pas.patternsidp.domain.model.validators;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Target(FIELD)
@Retention(RUNTIME)
@Constraint(validatedBy = BirthValidator.class)
public @interface ValidateBirth {
  public abstract int min() default 0;

  public abstract int max() default 0;
  public abstract String message() default "Message";
  public abstract Class<?>[] groups() default {};
  public abstract Class<? extends Payload>[] payload() default {};
}
