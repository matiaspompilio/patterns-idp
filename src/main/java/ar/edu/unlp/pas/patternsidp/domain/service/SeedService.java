package ar.edu.unlp.pas.patternsidp.domain.service;

import ar.edu.unlp.pas.patternsidp.domain.model.Address;
import ar.edu.unlp.pas.patternsidp.domain.model.Person;
import ar.edu.unlp.pas.patternsidp.domain.model.Role;
import ar.edu.unlp.pas.patternsidp.domain.repository.AddressRepository;
import ar.edu.unlp.pas.patternsidp.domain.repository.PersonRepository;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class SeedService implements ApplicationRunner {

  @Autowired
  private final PersonRepository personRepository;
  private final PasswordEncoder passwordEncoder;

  @Override
  public void run(ApplicationArguments args) {
    Address ADDRESS_1 = new Address("Brown", 623, 1, "A", "Viedma", "Rio Negro", "Argentina");
    Address ADDRESS_2 = new Address("Volta", 546, null, null, "Buenos Aires", "Buenos Aires", "Argentina");
    Address ADDRESS_3 = new Address("Brown", 623, 3, "C", "Viedma", "Rio Negro", "Argentina");
    Address ADDRESS_4 = new Address("Volta", 546, null, null, "Buenos Aires", "Buenos Aires", "Argentina");
    Address ADDRESS_5 = new Address("9 de Julio", 323, null, null, "Buenos Aires", "Buenos Aires", "Argentina");
    Address ADDRESS_6 = new Address("Dorrego", 123, 2, "D", "Buenos Aires", "Buenos Aires", "Argentina");
    Address ADDRESS_7 = new Address("Volta", 666, null, null, "Cordoba", "Cordoba", "Argentina");
    Address ADDRESS_8 = new Address("Volta", 555, null, null, "San Luis", "San Luis", "Argentina");

    Role role1 = new Role("ROLE_USER");
    Role role2 = new Role("ROLE_ADMIN");
    Role role3 = new Role("ROLE_DELIVERY");

    final var password = passwordEncoder.encode("123456");

    personRepository.saveAll(List.of(
      new Person("Samuel", "Seller", LocalDate.of(1979, 4, 11), "vendedor@email.com", ADDRESS_1, Collections.singletonList(ADDRESS_2), password, List.of(role1)),
      new Person("Jerry", "Buyer", LocalDate.of(1997, 8, 7), "comprador@email.com", ADDRESS_5, Collections.singletonList(ADDRESS_6), password, List.of(role1)),
      new Person("Hayden", "Admin", LocalDate.of(1957, 11, 24), "admin@email.com", ADDRESS_3, Collections.singletonList(ADDRESS_4), password, List.of(role2)),
      new Person("Steven", "Delivery", LocalDate.of(1984, 4, 8), "delivery@email.com", ADDRESS_7, Collections.singletonList(ADDRESS_8), password, List.of(role3))
    ));
  }
}